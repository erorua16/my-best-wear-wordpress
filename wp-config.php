<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'aurore' );

/** MySQL database password */
define( 'DB_PASSWORD', 'simplon' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!9X_kPixPg8*&[Ix4jH9Jz)lsI;3O)K|1Na~9V*$NwGb+4B|/2CPv`)#bFjr^~H?' );
define( 'SECURE_AUTH_KEY',  '7ZC(!][*>>=~b?Gx(t+M{U`:Uhx|2z=%kY6f<(:6Vqp:,m=b3W9j6r>]}YyT4eHF' );
define( 'LOGGED_IN_KEY',    'KWG:^K*W?8#D7N@87mjsG&t`5z2>)dBY&+LgwJqrCA0dOUj)1bYsb6+/LIZrK-kw' );
define( 'NONCE_KEY',        'TO7:w7$~y$$:Qq~o ;/D~O-@9,1o3Ry$t.% $*(.%*0!OU/#[luLjN}9bv8MlmIk' );
define( 'AUTH_SALT',        'oL0`=^KwHG<pQ.G7in^8-$f/9#ioij[)r)K=2$oX+Pf0U5Xtu^<^_#`A.n@pW [(' );
define( 'SECURE_AUTH_SALT', 'Z :Fra5I:aYz}Poo8tECCf_tvC:yF77}ziL@cNIj6M-xTU[3}t,wg)F+/>H,j>TR' );
define( 'LOGGED_IN_SALT',   'GaeTT5Ap-zqAL=nS%cLhz9~kc1)+wEC*Dc;<Db;Nu%f}IPbJx;jE/uh/2ckst,`;' );
define( 'NONCE_SALT',       'CXVtu8sG!Y;^E~w=pO0PlS!R:[]ccQN?11/sf!+PXk[56Va&TIPP,j0m.FqH`*uP' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */

// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
