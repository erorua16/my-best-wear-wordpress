# my-best-wear-wordpress

My best wear est un site wordpress de vente en ligne. Nous proposons la vente de tshirts personnalisables ou déjà personnalisés. Le client aura la possibilité d'uploader une image de son choix pour personnalisé son tshirt (V2). Le paiment en ligne n'est pas proposé.

## Installation

 - télécharger le dossier
 - créer une base de données appellée "wordpress"
 - installer wordpress en local
 - éditez votre dossier /etc/hosts en ajoutant
```
  # Projet wordpress
  127.0.0.1   wear.local
```    
 - créer un dossier wear.conf dans /etc/apache2/sites-available/
 - éditez le dossier wear.conf en ajoutant
``` 
    <VirtualHost *:8000>

     ServerName wear.local
     DocumentRoot /Home/Fatoumata/Documents/wear/ (remplacer par le chemin vers votre dossier wear)

     <Directory /Home/Fatoumata/Documents/wear/>
            Options FollowSymLinks
            AllowOverride AllowOverride
            Require all granted
            </Directory>
     </VirtualHost>
```
 - pour aller sur le site, entrez http://wear.local:8000 dans la barre url de votre navigateur


