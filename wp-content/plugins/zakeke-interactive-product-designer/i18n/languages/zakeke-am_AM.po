# Translation of Zakeke - WooCommerce in Amharic
# This file is distributed under the same license as the Zakeke - WooCommerce package.
msgid ""
msgstr ""
"PO-Revision-Date: +0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.3.1\n"
"Language: am_ET\n"
"Project-Id-Version: Zakeke - WooCommerce\n"

#: includes/admin/class-zakeke-admin-order.php:61
msgid "Download customization files"
msgstr "ብጁ ፋይሎች አውርድ"

#: includes/admin/class-zakeke-admin-order.php:64
msgid "Customization files in processing"
msgstr "በማካሄድ ላይ ያሉ የማበጀት ፋይሎች"

#: includes/api/class-zakeke-rest-enabled-controller.php:51
msgid "Product id of the product that became customizable."
msgstr "ለግል ብጁ የተደረገ ምርቱ የምርት መታወቂያ."

#: includes/api/class-zakeke-rest-enabled-controller.php:62
msgid "Product id of the product that is no longer customizable."
msgstr "ከአሁን በኋላ ማበጀት የማይችለውን የምርት መታወቂያ ምርት."

#: includes/api/class-zakeke-rest-enabled-controller.php:75
msgid "Required to be true, as resource does not support trashing."
msgstr "መገልገያ ቆሻሻን የማይደግፍ እንደመሆኑ መጠን እውን መሆን አለበት."

#: includes/api/class-zakeke-rest-enabled-controller.php:94
#: includes/api/class-zakeke-rest-settings-controller.php:62
msgid "Sorry, you are not allowed to create resources."
msgstr "ይቅርታ, ንብረቶችን ለመፍጠር አልተፈቀደልዎትም."

#: includes/api/class-zakeke-rest-enabled-controller.php:111
msgid "Sorry, you are not allowed to delete this resource."
msgstr "ይቅርታ, ይህንን ንብረት ለመሰረዝ አልተፈቀደልዎትም."

#: includes/api/class-zakeke-rest-enabled-controller.php:191
msgid "Product id."
msgstr "የምርት መታወቂያ."

#: includes/api/class-zakeke-rest-settings-controller.php:113
msgid "Shop currency."
msgstr "ምንዛሬ ይግዙ."

#: includes/class-zakeke-cart.php:141 includes/class-zakeke-order.php:74
msgid "Customization Price"
msgstr "የማበጀት ዋጋ"

#. #-#-#-#-#  zakeke-interactive-product-designer-code.pot (Zakeke Interactive
#. Product Designer 1.0.8)  #-#-#-#-#
#. Plugin Name of the plugin/theme
#: includes/class-zakeke-integration.php:19
msgid "Zakeke Interactive Product Designer"
msgstr "ዚኬኪ ኢንተዬቲክ ዲዛይነር ነዳፊ"

#: includes/class-zakeke-integration.php:20
msgid "Integrate Zakeke into WooCommerce. These credentials will be used to allow the integration of Zakeke with your store. By entering your Zakeke credentials your store will be able to communicate with the Zakeke API. Please refer to the <a href=\"https://zakeke.zendesk.com/hc/en-us/sections/360004488294-WooCommerce-">Zakeke documentation</a> if you are unsure how to proceed"
msgstr "Zakeke ን ወደ WooCommerce ያዋህዱ. እነዚህ ምስክርነቶች የ Zakekeን ከሱቅዎ ጋር ለማዋሃድ ጥቅም ላይ ይውላሉ. የ Zakeke ምስክርነቶችዎን በማስገባት የእርስዎ መደብር ከ Zakeke ኤፒአይ ጋር መገናኘት ይችላሉ. እንዴት መቀጠል እንዳለብዎ እርግጠኛ ካልሆኑ <a href=\"https://zakeke.zendesk.com/hc/en-us/sections/360004488294-WooCommerce-"> Zakeke ሰነዳን </a> ይመልከቱ."

#: includes/class-zakeke-integration.php:44
msgid "Zakeke username"
msgstr "የ Zakeke ተጠቃሚ ስም"

#: includes/class-zakeke-integration.php:46
msgid "Your Zakeke account username. Need a Zakeke account? <a href=\"https://portal.zakeke.com/Admin/Register\" target=\"_blank\">Click here to get one</a>"
msgstr "የ Zakeke አካውንትዎ የተጠቃሚ ስም. የዞኬክ መለያ ያስፈልጋል? <a href=\"https://portal.zakeke.com/Admin/Register\" target=\"_blank\"> አንድ ለማግኘት እዚህ ጠቅ ያድርጉ </a>"

#: includes/class-zakeke-integration.php:50
msgid "Zakeke password"
msgstr "Zakeke ይለፍ ቃል"

#: includes/class-zakeke-integration.php:52
msgid "Your Zakeke account password"
msgstr "የ Zakeke መለያዎ ይለፍ ቃል"

#: includes/class-zakeke-integration.php:55
msgid "Force product customization"
msgstr "የምርት ማበጀት ያስገድዱ"

#: includes/class-zakeke-integration.php:58
msgid "Replace the \"Add to cart\" button with the \"Customize\" button for customizable products"
msgstr "ሊለወጡ የሚችሉ ምርቶች በ «ለማሻሻያ» አዝራርን ወደ «ወደ ጋሪ አክል» አዝራር ይቀይሩ"

#: includes/class-zakeke-integration.php:62
msgid "Debug Log"
msgstr "ማስታወሻ አርም"

#: includes/class-zakeke-integration.php:64
msgid "Enable logging"
msgstr "መግባትን አንቃ"

#: includes/class-zakeke-integration.php:66
msgid "Log events such as API requests"
msgstr "እንደ ኤፒአይ ጥያቄዎች ያሉ የምዝግብ ማስታወሻዎች መዝግብ"

#: includes/class-zakeke-order.php:71
msgid "Customization"
msgstr "ብጁ ማድረግ"

#: includes/class-zakeke-product-page.php:61
#: includes/class-zakeke-product-page.php:87
#: includes/class-zakeke-product-page.php:95
msgid "Customize"
msgstr "ብጁ አድርግ"

#: zakeke.php:91 zakeke.php:99
msgid "Cheatin&#8217; huh?"
msgstr "ካቲን?"

#. Plugin URI of the plugin/theme
msgid "https://www.zakeke.com/"
msgstr "Https://www.zakeke.com/"

#. Author URI of the plugin/theme
msgid "https://www.zakeke.com"
msgstr "Https://www.zakeke.com"

#. Author of the plugin/theme
msgid "Zakeke"
msgstr "ዚኬክ"

#: zakeke.php:225
msgid "View Zakeke documentation"
msgstr "የ Zakeke ሰነዶችን ይመልከቱ"

#: zakeke.php:226
msgid "Documentation"
msgstr "ሰነድ"

#: zakeke.php:227
msgid "Register to Zakeke"
msgstr "ወደ ዚኬክ ይመዝገቡ"

#: zakeke.php:228
msgid "Register to Zakeke to use the plugin"
msgstr "ተሰኪውን ለመጠቀም ወደ ዚኬክ ይመዝገቡ"

#: zakeke.php:246
msgid "View Zakeke settings"
msgstr "የ Zakeke ቅንብሮችን ይመልከቱ"

#: zakeke.php:247
msgid "Settings"
msgstr "ቅንብሮች"

#. Description of the plugin/theme
msgid "Innovative platform to let your customers to customize products in your e-store. Multi-language, mult-currency, 3D view and print-ready outputs."
msgstr "ደንበኞችዎ በ e-store ውስጥ ምርቶቻቸውን እንዲያስተካክሉ የሚያስችላቸው አዳዲስ መድረክ. ባለብዙ ቋንቋ, ባለ ብዙ ምንዛሬ, የ 3 ዲ እይታ እና የታተሙ ውፅዓቶች."