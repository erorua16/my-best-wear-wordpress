<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Zakeke_Designer Class.
 */
class Zakeke_Designer {

	/**
	 * Setup class.
	 */
	public static function init() {
        add_action( 'wp_enqueue_scripts', array( __CLASS__, 'register_scripts' ), 20 );
        add_shortcode( 'zakeke', __CLASS__ . '::output' );
		if ( ! self::should_show_designer() ) {
			return;
		}

		remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'add_to_cart_action' ), 20 );

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ), 20 );
		add_filter( 'template_include', array( __CLASS__, 'template_loader' ), 991100 );
	}

    private static function should_show_designer() {
        return (
            ((!empty($_REQUEST['zakeke_design']) && 'new' === $_REQUEST['zakeke_design'])
                || (!empty($_REQUEST['zakeke_design_edit'])))
            && !isset($_REQUEST['tc_cart_edit_key'])
        );
    }

    public static function register_scripts() {
        wp_register_style( 'zakeke-designer', get_zakeke()->plugin_url() . '/assets/css/frontend/designer.css',
            array(),  ZAKEKE_VERSION );

        wp_register_script(
            'zakeke-designer',
            apply_filters( 'zakeke_javascript_designer', get_zakeke()->plugin_url() . '/assets/js/frontend/designer.js' ),
            array( 'jquery' ),
            ZAKEKE_VERSION
        );
    }

	public static function enqueue_scripts() {
		wp_enqueue_style( 'zakeke-designer' );
		wp_enqueue_script( 'zakeke-designer' );
	}

	/**
	 * Load the Zakeke designer template.
	 *
	 * @param mixed $template
	 *
	 * @return string
	 */
	public static function template_loader( $template ) {
		return zakeke_template_loader('zakeke.php');
	}

    /**
     * Load the Zakeke configurator template.
     *
     * @return string
     */
    private static function template_loader_shortcode() {
        $file     = 'zakeke-designer.php';
        $template = locate_template( $file );
        if ( ! $template ) {
            $template = get_zakeke()->plugin_path() . '/templates/' . $file;
        }

        return $template;
    }

    public static function output ( $atts = array() ) {
        if ($atts == '' ) {
            $atts = array();
        }

        if ( ! isset( $atts['product_id'] ) ) {
            return '<-- Zakeke: product_id parameter not set --!>';
        }

        $product = wc_get_product( intval( $atts['product_id'] ) );
        if (!$product) {
            return '<-- Zakeke: product not found --!>';
        }

        self::enqueue_scripts();

        $final_atts = $atts;
        $final_atts['product'] = $product;

        ob_start();
        include self::template_loader_shortcode();
        return ob_get_clean();
    }
}

Zakeke_Designer::init();
