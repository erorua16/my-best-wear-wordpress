<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Zakeke support class for http://www.najeebmedia.com
 *
 * @package Zakeke/support
 */
class Zakeke_Support_PPOM {

	/**
	 * Hook in Zakeke support handlers.
	 */
	public static function init() {
		add_filter( 'ppom_cart_line_total', array( __CLASS__, 'price' ), 20, 3 );
	}

	/**
	 * Add the Zakeke customization price
	 *
	 * @param float $cart_line_total
	 * @param WC_Cart|array $cart
	 * @param array $values
	 *
	 * @return float
	 */
	public static function price( $cart_line_total, $cart, $values ) {
		if ( isset( $values['zakeke_data'] ) ) {
			$qty = 0;
			if ( isset( $values['ppom']['ppom_option_price'] ) ) {
				// Getting option price
				$option_prices = json_decode( stripslashes( $values['ppom']['ppom_option_price'] ), true );
				if ( $option_prices ) {
					foreach ( $option_prices as $option ) {
						if ( $option['apply'] == 'quantities' ) {
							$qty += (int) $option['quantity'];
						}
					}
				}
			}

			if ( $qty === 0 ) {
				if ( is_array( $cart ) ) {
					$qty = floatval( $cart['quantity'] );
				} else {
					foreach ( $cart->get_cart_item_quantities() as $cart_item_quantity ) {
						$qty += $cart_item_quantity;
					}
				}
			}

			return $cart_line_total + ( $values['zakeke_data']['price'] * $qty );
		}

		return $cart_line_total;
	}
}

Zakeke_Support_PPOM::init();
