<?php
/**
 * Plugin Name: Upload
 * Description: images upload plugin.
 * Author: Fatou
 */ 

function uploadMyFile () {
    $content = ''; /* create string variable */

    /* open the form tag */
    $content .= '<br/> <br/> <br/> <form method="post" action="'.plugin_dir_url(__FILE__).'process/" enctype="multipart/form-data">';

    $content .= '<input type="file" accept="image/png, image/jpeg, image/jpg" name="my_unique_file" /><br/> <br/> <br/> <br/>';

    $content .= '<input type="submit" name="uploadMyFile_submit" value="upload your image"/>';
    
    /* open the form tag */
    $content .= '</form>';

    return $content;
}
add_shortcode('uploadfile', 'uploadMyFile');
